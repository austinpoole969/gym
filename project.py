from flask import Flask, request, render_template, redirect, url_for, flash, jsonify,Markup
from time import *
import sys
from flask import session as login_session
import random, string
from controllers import * 
from oauth2client.client import flow_from_clientsecrets
from oauth2client.client import FlowExchangeError
import httplib2
import json
from flask import make_response
import requests
from controllers import *
import lepl.apps.rfc3696
from passlib.hash import sha256_crypt
from msilib.schema import AdminExecuteSequence
import os
from werkzeug import secure_filename
import subprocess
from flask_mail  import Mail,Message

app = Flask(__name__)

app.config.update(dict(
    DEBUG = True,
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 587,
    MAIL_USE_TLS = True,
    MAIL_USE_SSL = False,
    MAIL_USERNAME = 'bpoole1835@gmail.com',
    MAIL_PASSWORD = 'Chessmaster1!',
    MAIL_DEFAULT_SENDER ='freund@gmail.com'
))
mail = Mail(app)
app.jinja_env.add_extension('jinja2.ext.do')

CLIENT_ID = json.loads(
    open('client_secrets.json', 'r').read())['web']['client_id']
APPLICATION_NAME = "Category App"

UPLOAD_FOLDER = 'websiteupload'
UPLOAD_PROFILE_PICTURE_MAX_SIZE=2000000
ALLOWED_EXTENSIONS = set(['bmp', 'png', 'jpg', 'jpeg','ppm', 'pgm', 'pbm',  'pnm','bpg'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route("/sup")
def index():

    msg = Message("Hello",
                  sender="from@example.com",
                  recipients=["poolebryan@comcast.net"])
    msg.html = "<b>testing</b>"
    mail.send(msg)
    return "hey"
def allowed_file_extension(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
def allowed_file_size(file):
    #By magic this will get the size of the file
    file.seek(0, os.SEEK_END)
    size=file.tell()
     #Must go back to the beginning of the file then save it
    file.seek(0)
    if UPLOAD_PROFILE_PICTURE_MAX_SIZE>size:
        return True
    else:
        return False
   
def filterListOnto(on, to):
    list=[]
    for onIndex in on:
        notIn=True
        for toIndex in to:
            if toIndex.user_id ==onIndex.user_id:
                notIn=False
        if notIn:    
            list.append(onIndex)
    return list   
   
def uploadProfilePic(file,user_id):    
        picS3=None
        if file and allowed_file_extension(file.filename) and allowed_file_size(file):
            filename = secure_filename(file.filename)
            abfile=os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(abfile)
            print abfile
            #aws s3 sync websiteupload\Now_Son.png s3://gympictures --acl public-read
            b=subprocess.Popen(["aws","s3","rm","s3://gymresources/users/"+str(user_id)+"/profile_pic", "--recursive"],shell=True,stdout=subprocess.PIPE)
            b.wait()
            picS3="s3://gymresources/users/"+str(user_id)+"/profile_pic/"+filename
            a=subprocess.Popen(["aws","s3","cp",abfile,picS3, "--acl","public-read"],shell=True,stdout=subprocess.PIPE)
            a.wait()
            os.remove(abfile)
            return 'https://s3.amazonaws.com/gymresources/users/'+str(user_id)+'/profile_pic/'+filename
        else:
            return None
                
@app.route('/testers',methods=['POST','GET'])
def teste():
    if request.method=="POST":
        file = request.files['file']
        if file and allowed_file_extension(file.filename) and allowed_file_size(file):
            filename = secure_filename(file.filename)
            abfile=os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(abfile)
            print abfile
            #aws s3 sync websiteupload\Now_Son.png s3://gympictures --acl public-read
            a=subprocess.Popen(["aws","s3","cp",abfile,"s3://gympictures", "--acl","public-read"],shell=True,stdout=subprocess.PIPE)
            a.wait()
            return redirect(url_for('trainerHome'))
    if request.method=="GET":
        output="<form action=\"testers\" method=\"POST\" enctype=multipart/form-data>\
              <p><input type=file name=file> \
              <input type=submit value=Upload>\
              </form>"
        return output

# Login in method for site
@app.route('/login')
def showLogin():
    state = ''.join(random.choice(string.ascii_uppercase + string.digits)
                   for x in xrange(32))
    login_session['state'] = state
    return render_template('TrainerPages/login.html', STATE=state)

def assignLoginSession(user,provider):
    if provider=="native":
        login_session['user_id']=user.user_id
        login_session['username']=user.username
        login_session['email']=user.email
        login_session['picture']=user.pictures
        login_session['provider']="native"
        login_session['privileges']={}
        login_session['privileges']['isAdmin']=user.isAdmin
        login_session['privileges']['isTrainer']=user.isAdmin
    if provider=='facebook':
        login_session['user_id'] = user.user_id
        login_session['username'] = user.username
        login_session['email']=user.email
        login_session['facebook_id'] = user.facebook_id
        login_session['provider'] = "facebook"
        login_session['picture']=user.pictures
        login_session['privileges']={}
        login_session['privileges']['isAdmin']=user.isAdmin
        login_session['privileges']['isTrainer']=user.isTrainer


@app.route("/native/login", methods=['POST'])
def nativeLogin():
    
    if request.method=='POST':
        
        user=None
        if 'identity' in request.form:
            
            email_validator = lepl.apps.rfc3696.Email()
            if email_validator(request.form['identity']):
                user=getUserByEmail(request.form['identity'])
            elif validUsername(request.form['identity']):
                user=getUserByUsername(request.form['identity'])
            else:
                flash("Incorrect Username/email or password")
                return redirect(url_for('showLogin'))
        else:
                flash("Incorrect Username/email or password")
                return redirect(url_for('showLogin'))
        
        if user is not None and 'pass' in request.form:
            if user.password=="" or user.password==None:
                
                flash("You haven't provided a password for this account. Login in via facebook or google plus")
                return redirect(url_for('showLogin')) 
            print user.password
            validated=sha256_crypt.verify(request.form['pass'],user.password)
            
            if validated:

                # I'm doing it this way so I can look at one method to change for all logins
                assignLoginSession(user,"native")

                return redirect(url_for('trainerHome'))
            else:
                flash("Incorrect Username/email or password")
                return redirect(url_for('showLogin'))
        else:
                flash("Incorrect Username/email or password")
                return redirect(url_for('showLogin'))        
@app.route("/native/logout",methods=['GET'])
def nativeLogout():
    if 'user_id' not in login_session or login_session['username'] == '':
        return redirect(url_for('trainerHome'))
    del login_session['username']
    del login_session['email']
    del login_session['picture']
    del login_session['user_id']
    del login_session['provider']
    del login_session['privileges']
    flash("You've been logged out")
    return redirect(url_for('trainerHome'))                
            
                

# For those logging in using gmail
@app.route('/gconnect', methods=['POST'])
def gconnect():
    # Validate state token
    if request.args.get('state') != login_session['state']:
        response = make_response(json.dumps('Invalid state parameter.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    # Obtain authorization code
    code = request.data

    try:
        # Upgrade the authorization code into a credentials object
        oauth_flow = flow_from_clientsecrets('client_secrets.json', scope='')
        oauth_flow.redirect_uri = 'postmessage'
        credentials = oauth_flow.step2_exchange(code)
    except FlowExchangeError:
        response = make_response(
            json.dumps('Failed to upgrade the authorization code.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    # Check that the access token is valid.
    access_token = credentials.access_token
    url = ('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s'
           % access_token)
    h = httplib2.Http()
    result = json.loads(h.request(url, 'GET')[1])
    # If there was an error in the access token info, abort.
    if result.get('error') is not None:
        response = make_response(json.dumps(result.get('error')), 500)
        response.headers['Content-Type'] = 'application/json'

    # Verify that the access token is used for the intended user.
    gplus_id = credentials.id_token['sub']
    if result['user_id'] != gplus_id:
        response = make_response(
            json.dumps("Token's user ID doesn't match given user ID."), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Verify that the access token is valid for this app.
    if result['issued_to'] != CLIENT_ID:
        response = make_response(
            json.dumps("Token's client ID does not match app's."), 401)
        print "Token's client ID does not match app's."
        response.headers['Content-Type'] = 'application/json'
        return response

    stored_credentials = login_session.get('credentials')
    stored_gplus_id = login_session.get('gplus_id')
    if stored_credentials is not None and gplus_id == stored_gplus_id:
        response = make_response(json.dumps('Current user is already connected.'),
                                 200)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Store the access token in the session for later use.
    login_session['credentials'] = access_token
    login_session['gplus_id'] = gplus_id

    # Get user info
    userinfo_url = "https://www.googleapis.com/oauth2/v1/userinfo"
    params = {'access_token': credentials.access_token, 'alt': 'json'}
    answer = requests.get(userinfo_url, params=params)

    data = answer.json()
    login_session['provider'] = 'google'
    login_session['username'] = data['name']
    login_session['picture'] = data['picture']
    login_session['email'] = data['email']
    user_id=getUserProfile(login_session['email'])
    
    if user_id == None:
        user_id=createUserProfile(login_session)
    login_session['user_id']=user_id
    output = ''
    output += '<h1>Welcome, '
    output += login_session['username']
    output += '!</h1>'
    output += '<img src="'
    output += login_session['picture']
    output += ' " style = "width: 300px; height: 300px;border-radius: 150px;-webkit-border-radius: 150px;-moz-border-radius: 150px;"> '
    flash("you are now logged in as %s" % login_session['username'])
    print "done!"
    print output
    return output



# For those logging in using facebook
@app.route('/fbconnect', methods=['POST'])
def fbconnect():
    if request.args.get('state') != login_session['state']:
        response = make_response(json.dumps('Invalid state parameter.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    access_token = request.data
    print "access token received %s " % access_token

    app_id = json.loads(open('fb_client_secrets.json', 'r').read())[
        'web']['app_id']
    app_secret = json.loads(
        open('fb_client_secrets.json', 'r').read())['web']['app_secret']
    url = 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=%s&client_secret=%s&fb_exchange_token=%s' % (
        app_id, app_secret, access_token)
    h = httplib2.Http()
    result = h.request(url, 'GET')[1]

    # Use token to get user info from API
    userinfo_url = "https://graph.facebook.com/v2.2/me"
    # strip expire tag from access token
    token = result.split("&")[0]


    url = 'https://graph.facebook.com/v2.5/me?%s' % token
    h = httplib2.Http()
    result = h.request(url, 'GET')[1]
    # print "url sent for API access:%s"% url
    # print "API JSON result: %s" % result
    data = json.loads(result)
    #login_session['username'] = data["name"]+'_'+data["id"]  # No longer doing this
    #for key, value in data.iteritems() :
        #print key + " = " + value
    #sys.exit(0)
    #login_session['picture'] = 'graph.facebook.com/' + data["id"] + '/picture?type=large'
    
    # Checking if user already
    user = None
    newUser=False
    if 'email' not in data:
        user = getUserProfile(fb_id=data['id'])
    else:
        user = getUserProfile(email=data['email'])
    # see if user exists
    if user is None:
        user = createUserProfile(login_session)
        newUser=True
    assignLoginSession(user,'facebook')

    # The token must be stored in the login_session in order to properly logout, let's strip out the information before the equals sign in our token
    stored_token = token.split("=")[1]
    login_session['access_token'] = stored_token

    # Get user picture
    url = 'https://graph.facebook.com/v2.2/me/picture?%s&redirect=0&height=200&width=200' % token
    result =requests.get(url).json()
    #print result['data']    
    #login_session['picture']=result['data']['url']
    
    output = ''
    output += '<h1>Welcome, '
    output += user.username
    if newUser: #if the user is new on the login.html there is a if statement that will redirect them to edit user page
        output +="<!--edit-->"
    output += '!</h1>'
    '''output += '<img src="'
    
    output += user.pictures
    output += ' " style = "width: 300px; height: 300px;border-radius: 150px; \
    -webkit-border-radius: 150px;-moz-border-radius: 150px;"> '
    '''

    flash("Now logged in as %s" % login_session['username'])
    return output

# For those who are logging off from our site who logged in via facebook
@app.route('/fbdisconnect')
def fbdisconnect():
    if 'username' not in login_session or login_session['username'] == '':
        return redirect(url_for('trainerHome'))
    facebook_id = login_session['facebook_id']
    # The access token must me included to successfully logout
    access_token = login_session['access_token']
    url = 'https://graph.facebook.com/%s/permissions?access_token=%s' % (facebook_id, access_token)
    h = httplib2.Http()
    result = h.request(url, 'DELETE')[1]
    del login_session['facebook_id']
    del login_session['username']
    del login_session['email']
    del login_session['picture']
    del login_session['user_id']
    del login_session['provider']
    del login_session['privileges']
    flash("You've been logged out")
    return redirect(url_for('trainerHome'))

# If the social media account is not associated
def createUserProfileFromSM(data,provider):
    newUser = None
    if provider == 'facebook':
        createUser(username=data["name"]+'_'+data["id"] ,pictures=login_session['picture'], facebook_id=data['id'])
        return getUserbyFacebookId(data['id'])
    return None

# get the user's info if we have it
def getUserInfo(user_id):
    user = session.query(User).filter_by(id=user_id).one()
    return user

# get that users id
def getUserProfile(email=None, fb_id=None):
    try:
        user = None
        if email:
            return getUserByEmail(email)
        if fb_id:
            return getUserbyFacebookId(fb_id)
        return user
    except:
        return None

@app.route('/gdisconnect')
def gdisconnect():
    if 'username' not in login_session or login_session['username'] == '':
        return redirect(url_for('showCategories'))
        # Only disconnect a connected user.
    credentials = login_session.get('credentials')
    if credentials is None:
        response = make_response(
            json.dumps('Current user not connected.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    access_token = credentials
    url = 'https://accounts.google.com/o/oauth2/revoke?token=%s' % access_token
    h = httplib2.Http()
    result = h.request(url, 'GET')[0]

    if result['status'] == '200':
        # Reset the user's sesson.
        del login_session['credentials']
        del login_session['gplus_id']
        del login_session['username']
        del login_session['email']
        del login_session['picture']
        del login_session['user_id']
        del login_session['provider']

        response = make_response(json.dumps('Successfully disconnected.'), 200)
        response.headers['Content-Type'] = 'application/json'
        return redirect(url_for('showCategories'))
    else:
        # For whatever reason, the given token was invalid.
        response = make_response(
            json.dumps('Failed to revoke token for given user.', 400))
        response.headers['Content-Type'] = 'application/json'
        return response


@app.route('/')
@app.route('/trainer/')
def trainerHome():
    if request.method == 'GET':
        sd="2015-10-05"
        sess=getAllSessionRange()
        list=[]
        dates={}
        bigString=""
        for s in sess:
            if s[0].date not in dates:
                dates[s[0].date]=[]
            dates[s[0].date].append('<h2> '+str(s[0].name)+'</h2>' \
                                 '<p><ul><li>Trainer: '+str(s[1].first_name)+' '+str(s[1].last_name)+'</li>'
                                 '<li>times:'+str(s[0].start_time.strftime("%I:%M%p")).lstrip("0")+'-'+str(s[0].end_time.strftime("%I:%M%p")).lstrip("0")+'</li>' \
                                 '<li>Goto:<a href=\\"'+url_for('showSessions',session_id=s[0].session_id)+'\\">View Session</a></li>'
                                 '</ul><\/p>')
        for key in dates:
            bigString+='{"date":"'+str(key)+'","badge":true,"title":"'+str(key.strftime("%A %B %d, %Y"))+'",'\
            '"body":"'+''.join(dates[key])+'","footer":"Nope",'\
            'modal: true},'
        if bigString != "" :
               bigString=bigString[:-1]
               bigString="["+bigString+"]"
        else: 
             bigString="[]"
        bigString=Markup(bigString) 
        #return jsonify(a=[{"a":"d"},{"c":"b"}])
        return render_template('TrainerPages/index.html',sess=sess,list=bigString)
    
@app.route("/calendar/")
def calendar():
    if request.method == 'GET':
        sess=[]
        user=None
        print 'user_id' in login_session
        if 'mine' in request.args and 'user_id' in login_session:
            if login_session['privileges']['isTrainer']==1:
                sess=getAllSessionbyTrainer(login_session['user_id'])
                user=getUserById(login_session['user_id'])
            elif login_session['privileges']['isTrainer']==0 and login_session['privileges']['isAdmin']==0:
                sess=getAllSessionbyUser(login_session['user_id'])
                user=getUserById(login_session['user_id'])
        else:
            sess=getAllSessionRange()
        list=[]
        dates={}
        bigString=""
        for s in sess:
            if s[0].date not in dates:
                dates[s[0].date]=[]
            dates[s[0].date].append('<h2> '+str(s[0].name)+'</h2>' \
                                 '<p><ul><li>Trainer: '+str(s[1].first_name)+' '+str(s[1].last_name)+'</li>'
                                 '<li>times:'+str(s[0].start_time.strftime("%I:%M%p")).lstrip("0")+'-'+str(s[0].end_time.strftime("%I:%M%p")).lstrip("0")+'</li>' \
                                 '<li>Goto:<a href=\\"'+url_for('showSessions',session_id=s[0].session_id)+'\\">View Session</a></li>'
                                 '</ul><\/p>')
        for key in dates:
            bigString+='{"date":"'+str(key)+'","badge":true,"title":"'+str(key.strftime("%A %B %d, %Y"))+'",'\
            '"body":"'+''.join(dates[key])+'","footer":"Nope",'\
            'modal: true},'
        if bigString != "" :
               bigString=bigString[:-1]
               bigString="["+bigString+"]"
        else: 
             bigString="[]"
        bigString=Markup(bigString) 
        #return jsonify(a=[{"a":"d"},{"c":"b"}])
        return render_template('TrainerPages/calendar.html',sess=sess,list=bigString,user=user,date=now.astimezone(us_Eastern_tz).date())
            
@app.route('/session/<int:session_id>')
#@app.route('/categories/<name>')
def showSessions(session_id):
    if request.method == 'GET':
        sess=getSession(session_id)
        sess[0].exercises=sess[0].planned_exercises.split(";")  
        for ex in  range(len(sess[0].exercises)):
            print sess[0].exercises[ex]
        return render_template('TrainerPages/session.html',sess=sess)

# create Session
@app.route('/session/create', methods=['GET', 'POST'])
def newSession():
    if "user_id" not in login_session:
        return redirect(url_for('trainerHome'))
        flash("Please login under as an admin or trainer")
    if  login_session['privileges']['isAdmin']!=1 and login_session['privileges']['isTrainer'] !=1:
        return redirect(url_for("trainerHome"))
        flash("You are not an admin or a trainer")
    if request.method == 'POST':
        addToSession=True
        name=None
        date=None
        tName=None
        start_time=None
        end_time=None
        slots=None
        planned_exercises=None
        location=None
        notes=None
        
        if "name" in request.form:
            name=request.form['name']
        else:
            addToSession=False
            flash("Please add a Name")
        
        if "date" in request.form:
            date=request.form['date']
        else:
            addToSession=False
            flash("Please provide a valid date")
        
        if "tName" in request.form:
            tName=request.form['tName']
            if int(tName)!=login_session["user_id"] and login_session['privileges']['isAdmin']!=1 :
                addToSession=False
                flash("You can only assign sessions to yourself")
        else:
            addToSession=False
            flash("no trainer selected")
            
        
        if "start_time" in request.form:
            st=strptime(request.form['start_time'],"%I:%M %p")
            start_time=datetime(hour=st.tm_hour,minute=st.tm_min,year=1900,month=3,day=1).time()
        else:
            addToSession=False
            flash("Please provide a start time")
        if "end_time" in request.form:
            et=strptime(request.form['end_time'],"%I:%M %p")
            end_time=datetime(hour=et.tm_hour,minute=et.tm_min,year=1900,month=3,day=1).time()
        else:
            addToSession=False
            flash("Please provide an end time")
        if 'slots'  in request.form :
            slots=request.form["slots"]
        else:
            addToSession=False
            flash("Please add the number of slots available")
        
        if 'attendees' in request.form:
            if slots:
                slots=request.form["slots"]
                if int(slots)>len(request.form.getlist('attendees')):
                    addToSession=False
                    flash("You have too many people and not enough slots")
                
        if "planned_exercises" in request.form:
            list=request.form.getlist("planned_exercises")
            planned_exercises=""
            for ex in list:
                planned_exercises=planned_exercises+ex+";"
            
            print planned_exercises
                
        if "location" in request.form:
            location=request.form["location"]
        else:
            addToSession=False
            flash("Please add a location")
        if "notes" in request.form:
            notes=request.form["notes"]
        if addToSession:
            results=createSession(name=name,date=date,creator_id=tName,start_time=start_time,end_time=end_time,slots=int(slots),planned_exercises=planned_exercises,location=location,notes=notes)
            return redirect(url_for("trainerHome"))
        else:
            return redirect(url_for("newSession"))
        if results is not None:
            if isinstance(results, basestring):
                flash(results)
                return redirect(url_for('newSession'))
            s=""
            for a in results:
               s+=a.name 
            flash("OverLapping times with:"+str(a)+",")            
        return redirect(url_for('trainerHome'))
    if request.method == 'GET':
        
        trainers=getAllUsers(getTrainers=True)
        allUsers=getAllUsers(False, False)
        #notTrainerUsers=getAllUsersbyNotTrainer(sess[0].creator_id)
        #trainerUsers=getUserbyTrainer(sess[0].creator_id)
        
        return render_template('TrainerPages/createsession.html',trainers=trainers, allUsers=allUsers)


# Edits Session
@app.route('/session/<int:session_id>/edit', methods=['GET', 'POST'])
def editSession(session_id):
    if "user_id" not in login_session:
        return redirect(url_for('trainerHome'))
        flash("Please login under as an admin or trainer")
    if  login_session['privileges']['isAdmin']!=1 and login_session['privileges']['isTrainer'] !=1:
        return redirect(url_for("trainerHome"))
        flash("You are not an admin or a trainer")
        
    #validate that its either the admin or the trainer themselves editing the session
    validateSess=getSession(session_id)
    if not validateSess[0].creator_id==login_session['user_id'] and not login_session['privileges']['isAdmin']==1:
        return redirect(url_for("trainerHome"))
        flash("You cannot edit this session because you are not the owner or admin ")
    if request.method == 'POST':
        addToSession=True
        name=None
        date=None
        tName=None
        start_time=None
        end_time=None
        slots=None
        planned_exercises=None
        location=None
        notes=None
        attendees=None
        if "name" in request.form:
            name=request.form['name']
        else:
            addToSession=False
            flash("missing name")
        
        if "date" in request.form:
            date=request.form['date']
        else:
            addToSession=False
            flash("Please provide a valid date")
        
        if "tName" in request.form:
            tName=request.form['tName']
            if int(tName)!=login_session["user_id"] and ['privileges']['isAdmin']!=1 :
                addToSession=False
                flash("You can only assign sessions to yourself")
        else:
            addToSession=False
            flash("Missing trainer")
            
        
        """if "start_time" in request.form:
            st=strptime(request.form['start_time'],"%I:%M %p")
            start_time=datetime(hour=st.tm_hour,minute=st.tm_min,year=1900,month=3,day=1).time()
        else:
            addToSession=False
            flash("provide a start time")
        if "end_time" in request.form:
            et=strptime(request.form['end_time'],"%I:%M %p")
            end_time=datetime(hour=et.tm_hour,minute=et.tm_min,year=1900,month=3,day=1).time()
        else:
            addToSession=False
            flash("Provide a end time")"""
        if 'slots'  in request.form :
            slots=request.form["slots"]
        else:
            addToSession=False
            flash("Missing slots")
        if 'attendees' in request.form:
            print len(request.form.getlist('attendees'))
            print "horse"
            if slots is not None and slots>=len(request.form.getlist('attendees')):
                addToSession=False
                flash("You have too many people and not enough slots")
        if "planned_exercises" in request.form:
            list=request.form.getlist("planned_exercises")
            planned_exercises=""
            for ex in list:
                planned_exercises=planned_exercises+ex+";"
            
            print planned_exercises
                
        if "location" in request.form:
            location=request.form["location"]
        else:
            flash("Provide a location")
            addToSession=False
            
        if "notes" in request.form:
            notes=request.form["notes"]
        print "hello"
        
        
            
        if addToSession:
            
            results=updateSession(session_id=session_id,name=name,date=date,creator_id=tName,start_time=start_time,end_time=end_time,slots=int(slots),planned_exercises=planned_exercises,location=location,notes=notes)
            if attendees:
                removeAddAllfromSession(attendees,session_id)
            return redirect(url_for("showSessions",session_id=session_id))
        else:
            flash("what I'm saying")
            return redirect(url_for("editSession",session_id=session_id))
        if results is not None:
            s=""
            for a in results:
                s+=a.name 
                flash("OverLapping times with:"+a+",")
                return redirect(url_for('editSession',session_id=session_id))
                        
        
        return redirect(url_for('showSessions',session_id=session_id))
    if request.method == 'GET':
        sess=getSession(session_id)
        sess[0].exercises=sess[0].planned_exercises.split(";")  
        
        trainers=getAllUsers(getTrainers=True)
        notTrainerUsers=filterListOnto(getAllUsersbyNotTrainer(sess[0].creator_id),sess[0].attendees)
        for t in getAllUsersbyNotTrainer(sess[0].creator_id):
            print t.first_name
        trainerUsers=filterListOnto(getUserbyTrainer(sess[0].creator_id),sess[0].attendees)
        exercises=getAllExercises()
        
        return render_template('TrainerPages/editsession.html',sess=sess,trainers=trainers, trainerUsers=trainerUsers,notTrainerUsers=notTrainerUsers,exercises=exercises)

#Deletes an Session
@app.route('/session/<int:session_id>/delete', methods=['GET', 'POST'])
def deleteSession(session_id):
    if "user_id" not in login_session:
        return redirect(url_for('trainerHome'))
        flash("Please login under as an admin or trainer")
    if  login_session['privileges']['isAdmin']!=1 and login_session['privileges']['isTrainer'] !=1:
        return redirect(url_for("trainerHome"))
        flash("You are not an admin or a trainer")
        
    #validate that its either the admin or the trainer themselves editing the session
    validateSess=getSession(session_id)
    if not validateSess[1].creator_id==login_session['user_id'] and not login_session['privileges']['isAdmin']==1:
        return redirect(url_for("trainerHome"))
    #if 'username' not in login_session or login_session['username'] == '':
        #print "why"
        #return "fap"
    #redirect(url_for('showCategories'))
    if request.method == 'POST':
       deleteSession(session_id)
       return redirect(url_for('trainerHome')) 
    
    if request.method == 'GET':
        sess=getSession(session_id)
        return render_template('TrainerPages/deletesession.html', sess=sess) 

@app.route('/user/clients/')
@app.route('/user/clients/<all>')
@app.route('/user/<trainer_name>/<int:user_id>/clients')
def showUsers(all=None,trainer_name=None,user_id=None):
    trainer=None
    if "user_id" not in login_session:
        flash("Please login under as an admin or trainer")
        return redirect(url_for('trainerHome'))
        
    if  login_session['privileges']['isAdmin']!=1 and login_session['privileges']['isTrainer'] !=1:
        flash("You are not an admin or a trainer")
        return redirect(url_for("trainerHome"))
        
    if request.method=='GET':
        if "mine" in request.args and login_session['privileges']['isTrainer'] ==1:
            users=getUserbyTrainer(login_session['user_id'])
            trainer=getUserById(login_session['user_id'])
        elif user_id is not None:
            users=getUserbyTrainer(user_id)
            trainer=getUserById(user_id)
        else:
            users=getAllUsers(getAdmin=0,getTrainers=0)
            for num in range(0,len(users)):
                print 
                trainerInfo=getTrainerbyUser(users[num].user_id)
                if trainerInfo is not None:
                    users[num].trainer_name=trainerInfo.first_name+" "+trainerInfo.last_name
                else:
                    users[num].trainer_name=None
                
        return render_template('TrainerPages/showclients.html',users=users,trainer=trainer)
@app.route('/user/trainers/')
def showTrainers():
    if request.method=='GET':
        users=getAllUsers(getTrainers=1)
        return render_template("TrainerPages/showtrainers.html",users=users)
    
# create User
@app.route('/user/create', methods=['GET', 'POST'])
def newUser():
    if request.method == 'POST':
        
        
        #Validate form in case of Assholery
        addUser=True
        first_name=""
        last_name=""
        middle_name=""
        username=None
        password=None
        checkPass=None
        phoneNumber=""
        email=""
        file = None
        picLocation=None
        if 'pic' in request.files:
            file=request.files['pic']
            if not file or not allowed_file_extension(file.filename) or not allowed_file_size(file):
                flash("the file you've uploaded must be an image( not a gif) and is smaller than 2 Megabytes")
                addUser=False
        if 'first_name' in request.form and request.form['first_name'].strip()!="":
            first_name=request.form['first_name'].strip()
            if not first_name.isalpha():
                flash("First Name can only contain letters")
                addUser=False
        if 'last_name' in request.form and request.form['last_name'].strip()!="":
            last_name=request.form['last_name'].strip()
            if not last_name.isalpha():
                flash("Last Name can only contain letters")
                addUser=False
        if 'middle_name' in request.form and request.form['middle_name'].strip()!="":
            middle_name=request.form['middle_name'].strip()
            if not middle_name.isalpha():
                flash("Middle Name can only contain letters")
                addUser=False
        if 'username' in request.form and request.form['username'].strip()!="":
            username=request.form['username'].strip()
            if not username.isalnum():
                flash("Username can only contain letters and numbers")
                addUser=False
            if not isUsernameOpen(username):
                flash("Username "+username+ " is not available")
                addUser=False
        else:
            addUser=False
            flash("You must have a user name")
        if 'password' in request.form:
            password=request.form['password']
            answer=validPassword(password)
            if len(answer)>0:
                for a in answer:
                    flash(a)
                addUser=False
            else:
                if 'checkPass' in request.form:
                    if not request.form['checkPass']==password:
                        addUser=False
                        flash("Passwords do not match")
                else:
                    addUser=False
                    flash("passwords do not match")
                password = sha256_crypt.encrypt(password)
        if 'email' in request.form  and request.form['email'].strip()!="":
            email=request.form['email']
            email_validator = lepl.apps.rfc3696.Email()
            if not email_validator(email):
                flash("invalid email")
                addUser=False
        if all((w in request.form  and w.strip()!="" for w in ["phone1", "phone2", "phone3"])):
            phoneNumber=request.form['phone1']+""+request.form['phone2']+""+request.form['phone3']
            if(len(phoneNumber)!=10):
                addUser=False
                flash("Please provide a phone number that is exactly 10 digits long")
            if(not phoneNumber.isnumeric()):
                addUser=False
                flash("Please user only numbers in your phone number")
                    
        if not addUser:
            return redirect(url_for('newUser'))
        else:
            createUser(first_name=first_name,last_name=last_name,middle_name=middle_name,username=username,password=password,phoneNumber=phoneNumber,email=email)
            newUserId=user_id=getUserByUsername(username).user_id
            picLocation=uploadProfilePic(file,newUserId)
            print picLocation
            updateUser(user_id=newUserId,pictures=picLocation)
            if 'privileges' in login_session and login_session["privileges"]["isAdmin"]==1 and "trainer" in request.form:
                trainer_id=int(request.form['trainer'])
                if validateUser(trainer_id):
                    assignUserToTrainer(user_id,trainer_id)
            elif 'privileges' in login_session and login_session["privileges"]["isTrainer"]==1:
                assignUserToTrainer(user_id,login_session["user_id"])
                    
            return redirect(url_for('trainerHome'))
            
    if request.method == 'GET':
        trainers=getAllUsers(getTrainers=1) 
        print trainers      
        return render_template('TrainerPages/createprofile.html',trainers=trainers)
# ---------------------------------------------------------------------------------------------------- Staff

@app.route('/admin/')
def opAdminPage():
    if request.method=='GET':
        if "user_id" not in login_session :
            return redirect(url_for("trainerHome"))  
        if not login_session['privileges']['isAdmin']==1:
            flash("You are not a trainer", category)
            return redirect(url_for("trainerHome"))
        return render_template("TrainerPages/opadminpage.html")
    

# create User
@app.route('/admin/staff/create', methods=['GET', 'POST'])
def newStaff():
    if "user_id" not in login_session :
        return redirect(url_for("trainerHome"))
        
        #validate that its either the admin or the user themselves editing the account   
    if not login_session['privileges']['isAdmin']==1:
        return redirect(url_for("trainerHome"))
    if request.method == 'POST':
        
        
        #Validate form in case of Assholery
        addUser=True
        first_name=""
        last_name=""
        middle_name=""
        username=None
        password=None
        checkPass=None
        phoneNumber=""
        email=""
        admin=0
        trainer=0
        file = None
        picLocation=None
        if 'pic' in request.files:
            file=request.files['pic']
            if not file or not allowed_file_extension(file.filename) or not allowed_file_size(file):
                flash("the file you've uploaded must be an image( not a gif) and is smaller than 2 Megabytes")
                addUser=False
        if 'admin' in request.form and request.form['admin'].strip()!="":
            admin=request.form['admin'].strip()
            if not admin.isnumeric() and admin!=1 and admin!=0:
                addUser=False
        if 'trainer' in request.form and request.form['trainer'].strip()!="":
            trainer=request.form['trainer'].strip()
            if not trainer.isnumeric() and trainer!=1 and trainer!=0:
                addUser=False
        if 'first_name' in request.form and request.form['first_name'].strip()!="":
            first_name=request.form['first_name'].strip()
            if not first_name.isalpha():
                flash("First Name can only contain letters")
                addUser=False
        if 'last_name' in request.form and request.form['last_name'].strip()!="":
            last_name=request.form['last_name'].strip()
            if not last_name.isalpha():
                flash("Last Name can only contain letters")
                addUser=False
        if 'middle_name' in request.form and request.form['middle_name'].strip()!="":
            middle_name=request.form['middle_name'].strip()
            if not middle_name.isalpha():
                flash("Middle Name can only contain letters")
                addUser=False
        if 'username' in request.form and request.form['username'].strip()!="":
            username=request.form['username'].strip()
            if not username.isalnum():
                flash("Username can only contain letters and numbers")
                addUser=False
            if not isUsernameOpen(username):
                flash("Username ",username, " is not available")
                addUser=False
        else:
            addUser=False
            flash("You must have a user name")
        if 'password' in request.form:
            password=request.form['password']
            answer=validPassword(password)
            if len(answer)>0:
                for a in answer:
                    flash(a)
                addUser=False
            else:
                if 'checkPass' in request.form:
                    if not request.form['checkPass']==password:
                        addUser=False
                        flash("Passwords do not match")
                else:
                    addUser=False
                    flash("passwords do not match")
                password = sha256_crypt.encrypt(password)
        if 'email' in request.form  and request.form['email'].strip()!="":
            email=request.form['email']
            email_validator = lepl.apps.rfc3696.Email()
            if not email_validator(email):
                flash("invalid email")
                addUser=False
        if all((w in request.form  and w.strip()!="" for w in ["phone1", "phone2", "phone3"])):
            phoneNumber=request.form['phone1']+""+request.form['phone2']+""+request.form['phone3']
            if(len(phoneNumber)!=10):
                addUser=False
                flash("Please provide a phone number that is exactly 10 digits long")
            if(not phoneNumber.isnumeric()):
                addUser=False
                flash("Please user only numbers in your phone number")    
        if not addUser:
            return redirect(url_for('newUser'))
        else:
            createUser(isTrainer=trainer,isAdmin=admin,first_name=first_name,last_name=last_name,middle_name=middle_name,username=username,password=password,phoneNumber=phoneNumber,email=email,pictures=picLocation)
            newUserId=user_id=getUserByUsername(username).user_id
            picLocation=uploadProfilePic(file,newUserId)
            updateUser(user_id=newUserId,pictures=picLocation)
            return redirect(url_for('trainerHome'))
            
    if request.method == 'GET':       
        return render_template('TrainerPages/createstaffer.html')
    
@app.route('/admin/staff/change_permissions/')
@app.route('/admin/staff/change_permissions/<user_type>/', methods=['GET', 'POST'])
@app.route('/admin/staff/change_permissions/<int:user_id>', methods=['GET', 'POST'])
def changePermissions(user_id=None,user_type=None):
    if "user_id" not in login_session :
        return redirect(url_for("trainerHome",session_id=session_id))
      
    #validate that its either the admin or the user themselves editing the account   
    if not login_session['privileges']['isAdmin']==1:
        return redirect(url_for("trainerHome"))
    
    if request.method=="POST":
        user=getUserById(user_id)
        print user
        if user_id==None or user==None:
            flash("The user doesn't exist")
            return redirect(url_for("trainerHome"))
        if 'info' in request.form :
            print request.form['admin']
            print request.form['trainer']
            return render_template('TrainerPages/changeperconfirmation.html',user=user,admin=int(request.form['admin']),trainer=int(request.form['trainer']))
        updateUs=True
        admin=None
        trainer=None
        if 'admin' in request.form:
            admin =int(request.form['admin'])
            if admin!=1 and admin!=0:
                flash("Hey admin send proper data")
                updateUs=False
        if 'trainer' in request.form:
            trainer =int(request.form['trainer'])
            if trainer!=1 and trainer!=0 and trainer!=2:
                flash("Hey admin send proper data")
                updateUs=False
        if updateUs:
            print "I'm not mad"
            updateUser(user_id=user_id,isAdmin=admin,isTrainer=trainer)  
            return redirect(url_for('changePermissions',user_type='clients'))     
        else:
            flash("user was not updated")
            return redirect(url_for('changePermissions',user_type='clients'))
                 
    if request.method=="GET":
        users=None
        if user_type=="trainer":
            users=getAllUsers(getTrainers=1)
        elif user_type=="client":
            users=getAllUsers(getAdmin=0,getTrainers=0)
        elif user_type=="admin":
            users=getAllUsers(getAdmin=1)
        else:
            users=getAllUsers()
        return render_template('TrainerPages/changepermissions.html',users=users)

# ------------------------------------------------------------------------------------   
def validPassword(password):
    answer=[]
    special = [ '?', '/', '<' ,'~' ,'#' ,'`', '!', '@', '$', '%', '^', '&', '*' ,'(', ')' ,'+' ,'=', '}', '|' ,':', '"', ';',  ' \''  , '>',',', '{', ' ', ']']
    hasSpecial=False
    if len(password)<=7:
        answer.append("password is must be 8 or more characters long")
    for s in special:
        if password.find(s)!=-1:
            hasSpecial=True
            break
    if not hasSpecial:
        answer.append("Please make sure you password contains at least one special character: ] [ ? / < ~ # ` ! @ $ % ^ & * ( ) + = } | : \" ; ' , > { space")
    return answer   
def validUsername(username):
    for cha in username:
        if not cha.isalnum() and cha!="-" and cha!="_":
            
            return False
    return True
@app.route("/user/account/")
@app.route("/user/account/<int:user_id>")
def showAccount(user_id=None):
    if 'user_id' not in login_session:
        return redirect(url_for("trainerHome"))
        flash("Please login first")
    if request.method=='GET':
        if user_id is not None and (login_session['privileges']['isAdmin']==1 or login_session['privileges']['isTrainer'] ==1 or login_session['user_id']==user_id): 
            user=getUserById(user_id)
            return render_template("TrainerPages/account.html",user=user )
        else:
            user=getUserById(login_session['user_id'])
            return render_template("TrainerPages/account.html",user=user)

@app.route("/user/account/edit")
@app.route("/user/account/edit/<int:user_id>",methods=['GET', 'POST'])
def editUser(user_id=None):
    if "user_id" not in login_session :
        return redirect(url_for("trainerHome"))
    if user_id is not None:
        #validate that its either the admin or the user themselves editing the account
        if not user_id==login_session['user_id'] and not login_session['privileges']['isAdmin']==1:
            return redirect(url_for("trainerHome"))
        
    if request.method == 'POST':
        
        
        #Validate form in case of Assholery
        user=getUserById(user_id)
        addUser=True
        first_name=None
        last_name=None
        middle_name=None
        username=None
        phoneNumber=""
        email=None
        file = None
        picLocation=None
        if 'pic' in request.files:
            file=request.files['pic']
            picLocation=uploadProfilePic(file,user_id)
            if(picLocation is None):
                flash("the file you've uploaded must be an image( not a gif) and is smaller than 2 Megabytes")
                addUser=False
                
        
        if 'first_name' in request.form and request.form['first_name'].strip()!="":
            first_name=request.form['first_name'].strip()
            if not first_name.isalpha():
                flash("First Name can only contain letters")
                addUser=False
        if 'last_name' in request.form and request.form['last_name'].strip()!="":
            last_name=request.form['last_name'].strip()
            if not last_name.isalpha():
                flash("Last Name can only contain letters")
                addUser=False
        if 'middle_name' in request.form and request.form['middle_name'].strip()!="":
            middle_name=request.form['middle_name'].strip()
            if not middle_name.isalpha():
                flash("Middle Name can only contain letters")
                addUser=False
        if 'username' in request.form and request.form['username'].strip()!="":
            username=request.form['username'].strip()
            print username
            if not validUsername(username):
                flash("Username can only contain letters, numbers, hypens, and underscores")
                addUser=False
            if user.username!=username:
                if not isUsernameOpen(username):
                    flash("Username "+username+ " is not available")
                    addUser=False
        if 'email' in request.form  and request.form['email'].strip()!="":
            email=request.form['email']
            email_validator = lepl.apps.rfc3696.Email()
            if not email_validator(email):
                flash("invalid email")
                addUser=False
            if user.email!=email:
                if not isEmailOpen(email):
                    flash("Email is already in use")
                    addUser=False
        if all((w in request.form  and w.strip()!="" for w in ["phone1", "phone2", "phone3"])):
            phoneNumber=request.form['phone1']+""+request.form['phone2']+""+request.form['phone3']
            if(len(phoneNumber)!=10):
                addUser=False
                flash("Please provide a phone number that is exactly 10 digits long")
            if(not phoneNumber.isnumeric()):
                addUser=False
                flash("Please user only numbers in your phone number")
        if not addUser:
            user=getUserById(user_id)
            return redirect(url_for('editUser',user=user))
        else:
            updateUser(user_id=user_id,first_name=first_name,last_name=last_name,middle_name=middle_name,username=username,phoneNumber=phoneNumber,email=email,pictures=picLocation)
            if user_id==login_session["user_id"]:
                assignLoginSession(getUserById(user_id), login_session['provider'])
            return redirect(url_for('showAccount',user_id=user_id))
    if request.method == 'GET':       
        if user_id is not None:
            user=getUserById(user_id)
            return render_template("TrainerPages/edituser.html",user=user )
        else:
            user=getUserById(login_session['user_id'])
            return render_template("TrainerPages/edituser.html",user=user)
@app.route("/user/account/delete/<int:user_id>")
def deleteUser(user_id):
    return ""

@app.route('/exercises/add', methods=['GET', 'POST'])
def addExerciseRoute():
    if request.method=="POST":
        print 
    if request.method=="GET":
        return render_template("TrainerPages/") 


#@app.route('/exercises/<name>', methods=['GET', 'POST'])
#@app.route('/exercises/<int:ex_id>', methods=['GET', 'POST'])
#def ():    
    
@app.route('/categories/add', methods=['GET', 'POST'])
def addCategory():
    if 'username' not in login_session or login_session['username'] == '':
        return redirect(url_for('showCategories'))
    if request.method == 'POST':
        check=session.query(Category).filter(Category.name==request.form['name'].lower())
        if check.count()>0:
            flash("This Category already exists")
            return redirect(url_for('showCategories'))
        newCat = Category(name=request.form['name'].lower(), \
                          description=request.form['description'], user_id=login_session['user_id'])
        session.add(newCat)
        session.commit()
        flash("New Category added")
        return redirect(url_for('showCategories'))
    if request.method == 'GET':
        return render_template('newCategory.html')

@app.route('/categories/category/<int:category_id>/delete', methods=['GET', 'POST'])
def deleteCategory(category_id):
    if 'username' not in login_session or login_session['username'] == '':
        return redirect(url_for('showCategories'))
    if request.method == 'POST':
        deletedCategory = delete(Category).where(Category.id == category_id)
        deletedItems = delete(Item).where(Item.category_id == category_id)
        session.execute(deletedItems)
        session.execute(deletedCategory)
        session.commit()
        return redirect(url_for('showCategories'))
    if request.method == 'GET':
        category = session.query(Category).filter(Category.id == category_id).first()
        items = session.query(Item).filter(Item.category_id == category_id)
        print category.id
        return render_template('deletecategory.html', category=category, items=items)

# @app.route('/newMenuItem')
# Creates new item
@app.route('/categories/item/new', methods=['GET', 'POST'])
def newItem():
    if 'username' not in login_session or login_session['username'] == '':
        return redirect(url_for('showCategories'))
    if request.method == 'POST':
        newItem = Item(title=request.form['title'], description=request.form['description'], price=request.form['price'], \
                       category_id=request.form['category_id'],user_id=login_session['user_id'], picture_url=request.form['picUrl'])
        session.add(newItem)
        session.commit()
        flash("new item created")
        return redirect(url_for('showCategories'))
    if request.method == 'GET':
        categories = session.query(Category).all()
        return render_template('newitem.html', categories=categories)


# Shows an item description
@app.route('/categories/<int:category_id>/<int:item_id>/', methods=['GET', 'POST'])
@app.route('/categories/<cat_name>/<item_name>/', methods=['GET', 'POST'])
def showItem(item_name=None, cat_name=None, category_id=None, item_id=None):    
    if request.method == 'GET':
        if item_id:
            categories = session.query(Category).all()
            item = session.query(Item).filter(Item.id == item_id).first()
        if item_name:
            categories = session.query(Category).all()
            item = session.query(Item).filter(Item.title == item_name).first()
        return render_template('Item.html', cat_name=cat_name, item=item)
    

# Task 3: Create a route for deleteMenuItem function here


@app.route('/catalog.json')
def catalogJSON():
    # return jsonify(add="fudge")
    # restaurant = session.query(Restaurant).filter_by(id=restaurant_id).one()
    categories = session.query(Category).all()
    f = []
    for cat in categories:
        s = cat.serialize
        temp = []
        items = session.query(Item).filter(Item.category_id == cat.id).all()
        for item in items:
            temp.append(item.serialize)
        s["items"] = temp
        f.append(s)
    return jsonify(Category=f)
   

        
if __name__ == '__main__':
    app.secret_key = 'super_secret_key'
    app.debug = True
    app.run(host='0.0.0.0', port=8000)
