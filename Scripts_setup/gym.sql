-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2015 at 11:41 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
Drop database if exists gym;
create database if not exists gym;
use gym;

--
-- Database: `gym`
--

-- --------------------------------------------------------


--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT ,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `middle_name` varchar(30) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL UNIQUE,
  `password` varchar(100) DEFAULT NULL,
  `isAdmin` int(11) NOT NULL, -- 1=active 0=no
  `isTrainer` int(11) NOT NULL, -- 1=active 0=no
  `DateStarted` date NOT NULL,
  `PhoneNumber` varchar(10) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL UNIQUE,
  `weight` float DEFAULT NULL,
  `Pictures` varchar(150) DEFAULT NULL,
  `facebook_id` varchar(100) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL, -- 1=male 2=Female 3=Idk/not answer
  `birth` date DEFAULT NULL,
  `status` int(11) not null, -- 1=active 0=inactive
  primary key (`user_id`)

) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `trainer_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `trainer_id` int(11) DEFAULT NULL,
  foreign key(user_id) references users(user_id),
  foreign key(trainer_id) references users(user_id),
  primary key (`user_id`, trainer_id)

) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL UNIQUE,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(15) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  foreign key(user_id) references users(user_id),
  primary key (`address_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `class_location` (
  `cl_id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(100) NOT NULL,
  `is_active` int (11) not null,
  primary key (`cl_id`)
  
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `session`
--


CREATE TABLE IF NOT EXISTS `session` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) NOT NULL,
  `cl_id` int(11),
  `name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `start_time` time,
  `end_time` time,
  `slots` int(11) Default NULL,
  `planned_exercises`  tinyblob NULL,
  `notes` mediumblob,
  `location` varchar(100) NOT NULL,
  `active` int(11) NOT NULL,
  primary key (`session_id`),
  foreign key(creator_id) references users(user_id),
  foreign key(cl_id) references class_location(cl_id)
  
  
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
-- http://wserver.flc.losrios.edu/~willson/fitnessHandouts/muscleGroups.html
CREATE TABLE IF NOT EXISTS `exercises` (
  `ex_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) unique,
  `video_link` varchar(250) DEFAULT NULL,
  `abdominal` int(11) DEFAULT NULL,
  `biceps` int(11) DEFAULT NULL,
  `deltoids` int(11) DEFAULT NULL,
  `erector_spinae` int(11) DEFAULT NULL,
  `gastrocnemius_soleus` int(11) DEFAULT NULL,
  `gluteus` int(11) DEFAULT NULL,
  `hamstrings` int(11) DEFAULT NULL,
  `latissimus_dorsi_rhomboids` int(11) DEFAULT NULL,
  `obliques` int(11) DEFAULT NULL,
  `pectoralis` int(11) DEFAULT NULL,
  `quadriceps`  int(11) DEFAULT NULL,
  `trapezius`  int(11) DEFAULT NULL,
  `triceps`  int(11) DEFAULT NULL,
  `full_body` int(11) DEFAULT NULL,
  `agility` int (11) DEFAULT NULL,
  `endurance` int (11) DEFAULT NULL,
  `is_active` int (11) not null,
  primary key (`ex_id`)

) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Table structure for table `session_user`
--

CREATE TABLE IF NOT EXISTS `session_user` (
  `session_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_shownup` int(11) DEFAULT NULL,
   primary key (`session_id`,`user_id`),
   foreign key(session_id) references `session`(session_id),
   foreign key(user_id) references `users`(user_id) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
-- ---------------------------- Inserting Trainers
INSERT INTO `gym`.`users`
(`first_name`,
`last_name`,
`username`,
`password`,
`isAdmin`,
`isTrainer`,
`DateStarted`,
`pictures`,
`status`,
`sex`
)
VALUES
('Nick',
'Smith',
'Nick',
'Nick1234',
0,
1,
CURDATE(),
'https://s3-us-west-2.amazonaws.com/gympictures/Defaults/blank_profile.png',
1,
1
);

INSERT INTO `gym`.`users`
(`first_name`,
`last_name`,
`username`,
`password`,
`isAdmin`,
`isTrainer`,
`DateStarted`,
`pictures`,
`status`,
`sex`
)
VALUES
('Arya',
'Grandai',
'Arya',
'Arya123',
0,
1,
CURDATE(),
'https://s3-us-west-2.amazonaws.com/gympictures/Defaults/blank_profile.png',
1,
1
);

INSERT INTO `gym`.`users`
(`first_name`,
`last_name`,
`username`,
`password`,
`isAdmin`,
`isTrainer`,
`DateStarted`,
`pictures`,
`status`,
`sex`
)
VALUES
('Kateric',
'Kateric',
'Kateric',
'',
1,
1,
CURDATE(),
'https://s3-us-west-2.amazonaws.com/gympictures/Defaults/blank_profile.png',
1,
1
);


-- ------------------------------ Inserting Customers
INSERT INTO `gym`.`users`
(`first_name`,
`last_name`,
`username`,
`password`,
`isAdmin`,
`isTrainer`,
`DateStarted`,
`pictures`,
`status`,
`sex`
)
VALUES
('Matthew Nicks Trainee',
'Alan',
'Alan1',
'Nick1234',
0,
0,
CURDATE(),
'https://s3-us-west-2.amazonaws.com/gympictures/Defaults/blank_profile.png',
1,
1
);

INSERT INTO `gym`.`users`
(`first_name`,
`last_name`,
`username`,
`password`,
`isAdmin`,
`isTrainer`,
`DateStarted`,
`pictures`,
`status`,
`sex`
)
VALUES
('Tom Aryas Trainee',
'Davis',
'Alan5',
'Nick1234',
0,
0,
CURDATE(),
'https://s3-us-west-2.amazonaws.com/gympictures/Defaults/blank_profile.png',
1,
1
);


INSERT INTO `gym`.`trainer_users`
(`user_id`,
`trainer_id`)
VALUES
(4,
1);

INSERT INTO `gym`.`trainer_users`
(`user_id`,
`trainer_id`)
VALUES
(5,
2);
/*
------------------------------------------- Creating Sessions
INSERT INTO `gym`.`session`
(`creator_id`,
`name`,
`date`,
`start_time`,
`end_time`,
`slots`,
`planned_exercises`,
`notes`,
`location`,
`active`)
VALUES
(1,
"Fun time",
"2015-10-17",
"18:30:00",
"19:00:00",
5,
"benchpress",
"nothing",
"gym",
1);

INSERT INTO `gym`.`session`
(`creator_id`,
`name`,
`date`,
`start_time`,
`end_time`,
`slots`,
`planned_exercises`,
`notes`,
`location`,
`active`)
VALUES
(1,
"Fun time",
"2015-10-17",
"18:00:00",
"18:30:00",
5,
"benchpress",
"nothing",
"gym",
1);
*/

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
