import os
import sys
from datetime import datetime,timedelta
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime,Float, LargeBinary,Time, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship,sessionmaker
from sqlalchemy import create_engine

# Necessary for the creation of our database.
Base = declarative_base()

class Users(Base):
    __tablename__='users'
    user_id=Column(Integer, primary_key=True)
    first_name=Column(String(30), nullable=True)
    last_name =Column(String(30), nullable=True, default="")
    middle_name=Column(String(30), nullable=True, default="")
    username =Column(String(100), nullable=True)
    password =Column(String(100), nullable=True)
    isAdmin= Column(Integer, nullable=False)
    isTrainer =Column(Integer, nullable=False)
    dateStarted =Column(DateTime, default=datetime.utcnow().date())
    phoneNumber =Column(String(10), nullable=True)
    email =Column(String(100), nullable=True)
    weight  =Column(Float, nullable=True)
    pictures =Column(String(250), nullable=True, default="https://s3-us-west-2.amazonaws.com/gympictures/Defaults/blank_profile.png")
    facebook_id=Column(String(100), nullable=True)
    sex = Column(Integer, nullable=False, default=3)
    birth=Column(Date, nullable =True)
    status= Column(Integer, nullable=False, default=1)
    @property
    def serialize(self):
        print
        return {
            'user_id': self.user_id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'middle_name': self.middle_name,
            'username': self.username,
            'password':self.password,
            'isAdmin':self.isAdmin,
            'isTrainer': self.isTrainer,
            'dateStarted':self.dateStarted,
            'phoneNumber':self.phoneNumber,
            'email':self.email,
            'weight':self.pictures,
            'facebook_id':self.facebook_id,
            'sex':self.sex,
            'birth':self.birth,
            'status':self.status
        }
        
class Trainer_Users(Base):
    __tablename__='trainer_users'
    user_id=Column(Integer, ForeignKey("users.user_id"),primary_key=True)
    trainer_id=Column(Integer,ForeignKey("users.user_id"), primary_key=True)

class Address(Base):
    __tablename__='address'
    address_id=Column(Integer, primary_key=True)
    user_id= Column(Integer,ForeignKey("users.user_id"), primary_key=True)
    street =Column(String(100), nullable=True)
    city =Column(String(100), nullable=True)
    state =Column(String(100), nullable=True)
    zip =Column(Integer, nullable=True)

class Session(Base):
    __tablename__='session'
    session_id= Column(Integer, primary_key=True)
    creator_id = Column(Integer,ForeignKey("users.user_id"), primary_key=True)
    name =Column(String(100), nullable=True)
    date =Column(Date, nullable=False, default=(datetime.utcnow()-timedelta(hours=4)).date())
    start_time =Column(Time, default=(datetime.utcnow()-timedelta(hours=4)).time())
    end_time= Column(Time, default=(datetime.utcnow()-timedelta(hours=3,minutes=30)).time())
    slots =Column(Integer, nullable=True)
    planned_exercises =Column(LargeBinary, nullable=True)
    notes =Column(LargeBinary, nullable=True)
    location=Column(String(100), nullable=False)
    active=Column(Integer, nullable=False, default=1)

class Session_User(Base):
    __tablename__='session_user'
    session_id=Column(Integer,ForeignKey("session.session_id"), primary_key=True)
    user_id =Column(Integer,ForeignKey("users.user_id"), primary_key=True)
    is_shownup =Column(Integer, nullable=True)
    
class Exercises(Base):
    __tablename__='exercises'
    ex_id= Column(Integer, primary_key=True )
    name= Column(String(100), nullable=False)
    video_link= Column(String(250), nullable=True)
    abdominal= Column(Integer, nullable=True, default=0)
    biceps= Column(Integer, nullable=True, default=0)
    deltoids= Column(Integer, nullable=True, default=0)
    erector_spinae= Column(Integer, nullable=True, default=0)
    gastrocnemius_soleus= Column(Integer, nullable=True, default=0)
    gluteus= Column(Integer, nullable=True, default=0)
    hamstrings =Column(Integer, nullable=True, default=0)
    latissimus_dorsi_rhomboids= Column(Integer, nullable=True, default=0)
    obliques= Column(Integer, nullable=True, default=0)
    pectoralis= Column(Integer, nullable=True, default=0)
    quadriceps=  Column(Integer, nullable=True, default=0)
    trapezius = Column(Integer, nullable=True, default=0)
    triceps=  Column(Integer, nullable=True, default=0)
    full_body =Column(Integer, nullable=True, default=0)
    agility =Column(Integer, nullable=True, default=0)
    endurance =Column(Integer, nullable=True, default=0)
    is_active= Column(Integer, default=1)
class Class_Locations(Base):
    __tablename__='class_locations'
    cl_id= Column(Integer, primary_key=True )
    location=Column(String(100), nullable=False)
    is_active= Column(Integer, default=1)
