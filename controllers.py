from datetime import datetime, timedelta
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, update, delete, desc, asc
from database_setup import Users, Address, Session, Session_User, Trainer_Users, Exercises, Class_Locations, Base
from sqlalchemy import create_engine
from sqlalchemy.sql.expression import Null
from pytz import timezone
import pytz
import pymysql
#Accounts for daylights savings
utc = timezone("UTC")
now = utc.localize(datetime.utcnow())
us_Eastern_tz = timezone('US/Eastern')
us_Eastern_time = now.astimezone(us_Eastern_tz)
print us_Eastern_time.date()
#thisDay=datetime(year=us_Eastern_time.year, month=us_Eastern_time.month, day=us_Eastern_time.day, hour=us_Eastern_time.hour, minute=us_Eastern_time.minute, second=us_Eastern_time.second, microsecond=us_Eastern_time.microsecond)



engine = create_engine('mysql+pymysql://bpoole6:Chessmaster1!@localhost/gym', pool_recycle=3600)
""",echo=True""" 
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

#datetime.datetime(year, month, day[, hour[, minute[, second[, microsecond[, tzinfo]]]]])

#print datetime(year=datetime.utcnow().year,day=datetime.utcnow().day,month=datetime.utcnow().month, hour=15,minute=8)-timedelta(days=365)
def validateSession(session_id):
    validate=session.query(Session).filter(Session.session_id==session_id).first()
    if validate:
        return True
    else :
        return False

def validateUser(user_id):
    validate=session.query(Users).filter(Users.user_id==user_id).first()
    if validate:
        return True
    else :
        return False

def validateExercise(ex_id):
    validate=session.query(Exercises).filter(Exercises.ex_id==ex_id).first()
    if validate:
        return True
    else :
        return False
    
# Crud operations user
#----------------------------------------------------------------------------------------------

def createUser(dateStarted=now.astimezone(us_Eastern_tz).date(), first_name="", last_name="", middle_name="", username="", password="", isAdmin=0, isTrainer=0, phoneNumber="", email="",
                weight=0, pictures=None,facebook_id=None,sex=None,birth=None,status=1):
    # Users(first_name=first_name)
    user=Users(dateStarted=dateStarted,first_name=first_name,
               last_name=last_name,middle_name=middle_name,username=username,password=password,
               isAdmin=isAdmin,isTrainer=isTrainer,phoneNumber=phoneNumber,
               email=email,weight=weight,pictures=pictures,facebook_id=facebook_id,sex=sex,birth=birth,status=status)
    session.add(user)
    session.commit()
    
def getUserById(user_id):
    user=session.query(Users).filter(Users.user_id==user_id).first()
    return user

def getAllUsers(getTrainers=None, getAdmin=None):
    users=session.query(Users)
    if getTrainers is not None:
        users= users.filter(Users.isTrainer==1) if (getTrainers==True)   else users.filter(Users.isTrainer==0)
    if getAdmin is not None:
        users=  users.filter(Users.isAdmin==1) if (getAdmin==True)   else users.filter(Users.isAdmin==0)
    
    users=users.all()
    return users

def getAllUsersbyNotTrainer(user_id):
    users=session.query(Users).join(Trainer_Users, Trainer_Users.user_id==Users.user_id).filter(Users.isTrainer==0,Trainer_Users.trainer_id<>user_id).all()
    return users

def getTrainerbyUser(user_id):
    client=session.query(Trainer_Users).join(Users,Trainer_Users.user_id==Users.user_id).filter(Trainer_Users.user_id==user_id).first()
    if client is None:
        return None
    trainer=getUserById(client.trainer_id)
    return trainer

#users=getAllUsers(getTrainers=0,getAdmin=0)
#for num in users:
    #print num
    #trainerInfo=getTrainerbyUser(users[num].user_id)        
    #users[num].trainer_name=trainerInfo.first_name," ",trainerInfo.last_name 

    
def getUserbyTrainer(user_id):    
    users=session.query(Users).join(Trainer_Users,Trainer_Users.user_id==Users.user_id).filter(Trainer_Users.trainer_id==user_id).all()
    return users

def isUsernameOpen(username):
    users=session.query(Users).filter(Users.username==username).first()
    if users is None:
        return True
    else:
        return False
def isEmailOpen(email):
    users=session.query(Users).filter(Users.email==email).first()
    if users is None:
        return True
    else:
        return False
#print getUserbyTrainer(1)


def updateUser(user_id,first_name="",last_name="",middle_name="",username="",isAdmin="",isTrainer="",phoneNumber="",email="",weight="",pictures=""):
    setList=[]
    if(first_name!=None and first_name !=""):
        setList.append("first_name='"+first_name+'\'')
        
    if(last_name!=None and last_name !=""):
        setList.append("last_name='"+last_name+'\'')
    if(middle_name!=None and middle_name !=""):
        setList.append("middle_name='"+middle_name+'\'')
    if(username!=None and username !=""):
        setList.append("username='"+username+'\'')
    if(isAdmin!=None and isAdmin !=""):
        setList.append("isAdmin="+str(isAdmin))
    if(isTrainer!=None and isTrainer !=""):
        setList.append("isTrainer="+str(isTrainer))
    if(phoneNumber!=None and phoneNumber !=""):
        setList.append("phoneNumber='"+phoneNumber+'\'')
    if(email!=None and email !=""):
        setList.append("email='"+email+'\'')
    if(weight!=None and weight !=""):
        setList.append("weight="+str(weight))
    if(pictures!=None and pictures !=""):
        setList.append("pictures='"+pictures+'\'')
    
    query="UPDATE Users set "
    for a in setList:
        query+=a+","
    query=query[:-1]
    query+= " WHERE user_id="+str(user_id)
    print query
    if len(setList)>0:
        conn=engine.connect()
        trans=conn.begin()
        result = conn.execute(query)
        trans.commit()
        conn.close()
        session.commit() # This "basically" refreshes the connection
    
    return None
    
"""

def deleteUser(user_id):
"""    
def getUserByEmail(email):
    users=session.query(Users).filter(Users.email==email).first()
    return users

def getUserByUsername(username):
    users=session.query(Users).filter(Users.username==username).first()
    return users

def getUserbyFacebookId(facebook_id):
    user=session.query(Users).filter(Users.facebook_id==facebook_id).first()
    return user

# Crud operations Sessions
#----------------------------------------------------------------------------------------------

# class datetime.datetime(year, month, day[, hour[, minute[, second[, microsecond[, tzinfo]]]]])
def createSession(name, creator_id, date,start_time,end_time, slots,  planned_exercises, notes, location):
    
    #if duration <=0:
    #    return "Duration can't be less than or equal to zero"
    #if end_date_time<start_date_time:
    #    return "End date time can't be less than the start date time"
    
    #print start_time.minute
    hour_diff= end_time.hour-start_time.hour
    minute_diff= end_time.minute-start_time.minute
    diff =hour_diff*60+ minute_diff
    if diff<10:
        return "The Duration of the session must be longer than 10 minutes"
    #   |  |
    # |  |
    #vv=end_date_time.time()
    #print vv
    case1 = session.query(Session).filter(Session.start_time < end_time, Session.end_time > end_time,
                                           Session.date == date, Session.location=="gym").all()
    
    # |  |
    #   |  |
    case2 = session.query(Session).filter(Session.start_time < start_time, Session.end_time > start_time,
                                           Session.date == date,Session.location=="gym").all()
    # | |
    # | |
    case3 = session.query(Session).filter(Session.start_time == start_time,
                                           Session.date == date,Session.location=="gym").all()
    
    #  | |
    #|     |                                  
    case4 = session.query(Session).filter(Session.start_time > start_time, Session.end_time < end_time,
                                           Session.date == date,Session.location=="gym").all()
    
    # |     |
    #   | |
    case5 = session.query(Session).filter(Session.start_time > start_time, Session.end_time > end_time,
                                           Session.date == date,Session.location=="gym").all()
    
    if case1:
        return case1
    if case2:
        return case2
    if case3:
        return case3
    if case4:
        return case4
    if case5:
        return case5
    
    #minumum duration is 10 minutes
    #You can have a starttime and endtime overlap
    #End time cannot be less than start time
    a = Session(creator_id=creator_id,
              name=name,
              date=date,
              start_time=start_time,
              end_time=end_time,
              slots=slots,
              planned_exercises=planned_exercises,
              notes=notes,
              location=location
              
              )
    session.add(a)
    session.commit()
    
    return None
#print createSession("name", 1, datetime.utcnow().date(),  (datetime.utcnow()-timedelta(hours=2)).time(), datetime.utcnow().time() , 5, "planned_exercises", "notes", "gym")

def availableSlots(session_id):
    slotsAvailiable=session.query(Session_User).filter(Session_User.session_id==session_id).count()
    return slotsAvailiable

def getSessionAttendees(session_id):
    sess_users=session.query(Users).filter(Session_User.session_id==session_id).join(Session_User,Users.user_id==Session_User.user_id).all()
    return sess_users

#print getSessionAttendees(1)
def getSession(session_id):
    sess =session.query(Session,Users).join(Users,Users.user_id==Session.creator_id).filter(Session.session_id==session_id).first()
    sess[0].slotsAvailable=sess[0].slots-availableSlots(session_id)
    sess[0].attendees=getSessionAttendees(session_id)
    return sess

def updateSession(session_id,name, creator_id, date,start_time,end_time, slots,  planned_exercises, notes, location,active=1):
    #print start_time.minute
    hour_diff= end_time.hour-start_time.hour
    minute_diff= end_time.minute-start_time.minute
    diff =hour_diff*60+ minute_diff
    if diff<10:
        return "The Duration of the session must be longer than 10 minutes"
    #   |  |
    # |  |
    #vv=end_date_time.time()
    #print vv
    case1 = session.query(Session).filter(Session.start_time < end_time, Session.end_time > end_time,
                                           Session.date == date, Session.location=="gym",Session.session_id<>session_id).all()
    
    # |  |
    #   |  |
    case2 = session.query(Session).filter(Session.start_time < start_time, Session.end_time > start_time,
                                           Session.date == date,Session.location=="gym",Session.session_id<>session_id).all()
    # | |
    # | |
    case3 = session.query(Session).filter(Session.start_time == start_time,
                                           Session.date == date,Session.location=="gym",Session.session_id<>session_id).all()
    
    #  | |
    #|     |                                  
    case4 = session.query(Session).filter(Session.start_time > start_time, Session.end_time < end_time,
                                           Session.date == date,Session.location=="gym",Session.session_id<>session_id).all()
    
    # |     |
    #   | |
    case5 = session.query(Session).filter(Session.start_time > start_time, Session.end_time > end_time,
                                           Session.date == date,Session.location=="gym",Session.session_id<>session_id).all()
    
    if case1:
        return case1
    if case2:
        return case2
    if case3:
        return case3
    if case4:
        return case4
    if case5:
        return case5
    #minumum duration is 10 minutes
    #You can have a starttime and endtime overlap
    #End time cannot be less than start time
    #update(Session).where(Session.session_id=session_id).values(creator_id)
    updated = update(Session).where(Session.session_id == session_id).values(creator_id=creator_id,
              name=name,
              date=date,
              start_time=start_time,
              end_time=end_time,
              slots=slots,
              planned_exercises=planned_exercises,
              notes=notes,
              location=location)
    
    session.execute(updated)
    session.commit()
    return None  


def deleteSession(session_id):
    deleted = update(Session).where(Session.session_id == session_id).values(active=0)
    session.execute(deleted)
    session.commit()


def getAllSessionRange(start_date=(datetime(year=datetime.utcnow().year,day=datetime.utcnow().day,month=datetime.utcnow().month, hour=15,minute=8)-timedelta(days=365)).date(),end_date=(datetime(year=datetime.utcnow().year,day=datetime.utcnow().day,month=datetime.utcnow().month, hour=15,minute=8)-timedelta(days=-365)).date()):
    sess=session.query(Session,Users).join(Users,Users.user_id==Session.creator_id).filter(Session.date>=start_date,Session.date<=end_date).order_by(asc(Session.start_time)).all()
    activeSess=[]
    for a in sess:
        
        if a[0].active==1:
            a[0].slotsAvailable=a[0].slots-int(availableSlots(a[0].session_id))
            #print "bd",a[0].slotsAvailable
            activeSess.append(a)
    return activeSess
    
def getAllSessionbyTrainer(creator_id,start_date=None,end_date=None):
    sess=None
    if(start_date==None or end_date==None):
        sess=getAllSessionRange()
    else:
        sess=getAllSessionRange(start_date, end_date)
    #print sess[0][0].session_id
    tSess=[]
    for a in sess:
        
        if a[0].creator_id==creator_id:
            tSess.append(a)
    
    return tSess
def getSessionTrainer(session_id):
    user=session.query(Users).join(Session,Users.user_id==Session.creator_id).filter(Session.session_id==session_id).first()
    return user
#print getSessionTrainer(1).first_name
#print getAllSessionbyTrainer(1)[0][1].first_name

def getAllSessionbyUser(user_id,start_date=None,end_date=None):
    sess=session.query(Session_User).filter(Session_User.user_id==user_id)
    return sess
            
# Crud operations session_user
#----------------------------------------------------------------------------------------------

def addUsersToSession(user_idList,session_id):
    for user_id in user_idList:
        su=Session_User(user_id=user_id,session_id=session_id)
        addUsersToSession(user_id, session_id)

def removeAddAllfromSession(user_idList,session_id):
    NotGoinganyLonger=session.query(Session_User).filter(~Session_User.user_id.in_(user_idList)).all()
    #Email those not going anyLonger
    deleted=delete(Session_User).where(Session_User.session_id==session_id)
    session.execute(deleted)
    session.commit()
    for user_id in user_idList:
        session.add(Session_User(session_id=session_id,user_id=user_id))
    
    session.commit()
    

def addUserToSession(user_id,session_id):
    #add a method to email and/or text user
    su=Session_User(session_id=session_id,
                    user_id=user_id)
    session.add(su)
    session.commit()

def removeUserFromSession(user_id,session_id):
    #add a method to email and/or text user
    #Update Waitlist
    deleted = delete(Session_User).where(Session_User.session_id == session_id,Session_User.user_id==user_id )
    session.execute(deleted)
    session.commit()


def userShownUpToSession(user_id,session_id,is_shownup):
    #add a method to email and/or text user
    updated=update(Session_User).where(Session_User.session_id==session_id, Session_User.user_id==user_id).values(is_shownup=is_shownup)
    session.execute(updated)
    session.commit()


def expandClass(session_id,addThismany):
    #add a method to email and/or text user
    slotnum=session.query(Session).filter(Session.session_id==session_id).first().slots
    newSlots=slotnum+addThismany
    updated=update(Session).where(Session.session_id==session_id).values(slots=newSlots)
    session.execute(updated)
    session.commit()
    
#print datetime.now()
 
# Crud operation for Trainer_User
#----------------------------------------------------------------------------------   

def assignUserToTrainer(user_id, trainer_id):
    tu=Trainer_Users(user_id=user_id,trainer_id=trainer_id)
    session.add(tu)
    session.commit()

# Crud operation for exercies 
#-----------------------------------------------------------------------------------

def getExercisebyId(ex_id):
    ex=session.query(Exercises).filter(Exercises.ex_id==ex_id).first()
    return ex

def getExercisebyName(name):
    ex=session.query(Exercises).filter(Exercises.name==name).first()
    return ex

def getAllExercises():
    ex=session.query(Exercises).filter(Exercises.is_active==1).all()
    return ex

def addExercise(name=None,video_link=None,abdominal=None,biceps=None,deltoids=None,erector_spinae=None,gastrocnemius_soleus=None,gluteus=None,hamstrings=None,latissimus_dorsi_rhomboids=None,obliques=None,pectoralis=None,quadriceps=None,trapezius=None,triceps=None,full_body=None,agility=None,endurance=None):
    addEx=Exercises(name=name,video_link=video_link,abdominal=abdominal,biceps=biceps,deltoids=deltoids,erector_spinae=erector_spinae,gastrocnemius_soleus=gastrocnemius_soleus,gluteus=gluteus,hamstrings=hamstrings,latissimus_dorsi_rhomboids=latissimus_dorsi_rhomboids,obliques=obliques,pectoralis=pectoralis,quadriceps=quadriceps,trapezius=trapezius,triceps=triceps,full_body=full_body,agility=agility,endurance=endurance)
    session.add(addEx)
    session.commit()


def updateExercise(ex_id,name=None,video_link=None,abdominal=None,biceps=None,deltoids=None,erector_spinae=None,gastrocnemius_soleus=None,gluteus=None,hamstrings=None,
                   latissimus_dorsi_rhomboids=None,obliques=None,pectoralis=None,quadriceps=None,trapezius=None,triceps=None,full_body=None,agility=None,endurance=None):
    setList=[]
    if(first_name!=None and first_name !=""):
        setList.append("first_name='"+first_name+'\'')
    if validateExercise(ex_id):
        if name!=None and name!="":
            setList.append("name='"+name+"'")
        if video_link!=None and video_link!="":
            setList.append("video_link='"+video_link+"'")
        if abdominal!=None and abdominal!="":
            setList.append("abdominal='"+abdominal+"'") 
        if biceps!=None and biceps!="":
            setList.append("biceps='"+biceps+"'")
        if deltoids!=None and nadeltoidsme!="":
            setList.append("deltoids='"+deltoids+"'")
        if erector_spinae!=None and erector_spinae!="":
            setList.append("erector_spinae='"+erector_spinae+"'")
        if gastrocnemius_soleus!=None and gastrocnemius_soleus!="":
            setList.append("gastrocnemius_soleus='"+gastrocnemius_soleus+"'")
        if gluteus!=None and gluteus!="":
            setList.append("gluteus='"+gluteus+"'")
        if hamstrings!=None and hamstrings!="":
            setList.append("hamstrings='"+hamstrings+"'")
        if latissimus_dorsi_rhomboids!=None and latissimus_dorsi_rhomboids!="":
            setList.append("latissimus_dorsi_rhomboids='"+latissimus_dorsi_rhomboids+"'")
        if obliques!=None and obliques!="":
            setList.append("obliques='"+obliques+"'")
        if pectoralis!=None and pectoralis!="":
            setList.append("pectoralis='"+pectoralis+"'")
        if quadriceps!=None and quadriceps!="":
            setList.append("quadriceps='"+quadriceps+"'")
        if trapezius!=None and trapezius!="":
            setList.append("trapezius='"+trapezius+"'")
        if full_body!=None and full_body!="":
            setList.append("full_body='"+full_body+"'")
        if full_body!=None and full_body!="":
            setList.append("full_body='"+full_body+"'")
        if agility!=None and agility!="":
            setList.append("agility='"+agility+"'")
        if endurance!=None and endurance!="":
            setList.append("endurance='"+endurance+"'")
        
        query="UPDATE exercise set "
        for a in setList:
            query+=a+","
        query=query[:-1]
        query+= " WHERE ex_id="+str(ex_id)
        print query
        if len(setList)>0:
            conn=engine.connect()
            trans=conn.begin()
            result = conn.execute(query)
            trans.commit()
            conn.close()
            session.commit() # This "basically" refreshes the connection

def deactivateExercise(ex_id):
    updated=update(Exercises).where(Exercises.ex_id==ex_id).values(is_active=0)
    session.execute(updated)
    session.commit()
#print validateUser(1)
# GRANT ALL PRIVILEGES ON dbTest.* To 'bpoole'@'hostname' IDENTIFIED BY '46Ugumuya'
# ^useful when you need to give a user permission
query = 'select sp.PicturePath, s.name, s.StripperID from stripperpictures sp inner join stripper s on s.mainPicId=sp.picID'
