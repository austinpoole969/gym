import os
import sys
from datetime import datetime 
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship,sessionmaker
from sqlalchemy import create_engine


# Necessary for the creation of our database.
Base = declarative_base()

# The user table
class address(Base):
    __tablename__='address'
    address_id= Column(Integer, primary_key=True)
    user_id =Column(Integer, nullable=False)
    street= Column(String(250), nullable=False)
    city= Column(String(250), nullable=False)
    state= Column(String(250), nullable=False)
    zip =Column(Integer, nullable=False)
    """name=Column(String(250), nullable=False)
    # Made Email nullable because if they sign in with facebook we might not be 
    # to pull it if they haven't validated their email.
    email=Column(String(250), nullable=True)
    picture=Column(String(250), nullable=False)
    facebook_id=Column(String(250), nullable=True)
    id=Column(Integer, primary_key=True)
    """
class session_user(Base):
    __tablename__='session_user'
    session_id=Column(Integer, primary_key=True)
    user_id=Column(Integer, nullable=False)
    is_shownup=Column(Integer,nullable=False)
    
engine = create_engine('mysql+pymysql://bpoole6:Chessmaster1!@localhost/gym', pool_recycle=3600) 
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

a= (session.query(session_user).all())
for i in range(0,len(a)):
    print a[i].user_id
# Login in method for s   
print datetime.utcnow().date()